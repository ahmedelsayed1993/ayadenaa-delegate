package com.aait.ayadenaadelegate.Models;

import java.util.ArrayList;

public class NewOrderResponse extends BaseResponse {
    private ArrayList<OrderModel> data;

    public ArrayList<OrderModel> getData() {
        return data;
    }

    public void setData(ArrayList<OrderModel> data) {
        this.data = data;
    }
}

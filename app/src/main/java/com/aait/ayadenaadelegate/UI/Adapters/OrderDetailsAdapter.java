package com.aait.ayadenaadelegate.UI.Adapters;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aait.ayadenaadelegate.Models.OrderProductModel;
import com.aait.ayadenaadelegate.R;
import com.aait.ayadenaadelegate.Base.ParentRecyclerAdapter;
import com.aait.ayadenaadelegate.Base.ParentRecyclerViewHolder;

import com.bumptech.glide.Glide;

import java.text.MessageFormat;
import java.util.List;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Mahmoud ibrahim on 12/2/2018.
 */

public class OrderDetailsAdapter extends ParentRecyclerAdapter<OrderProductModel> {

    public OrderDetailsAdapter(final Context context, final List<OrderProductModel> data, final int layoutId) {
        super(context, data, layoutId);
    }

    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        ViewHolder holder = new ViewHolder(itemView);
        holder.setOnItemClickListener(itemClickListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ParentRecyclerViewHolder holder, final int position) {
        final ViewHolder viewHolder = (ViewHolder) holder;
        OrderProductModel foodModel = data.get(position);



        viewHolder.tvItemName.setText(foodModel.getProduct_name());

        viewHolder.tvItemPrice.setText(foodModel.getProduct_price()+ mcontext.getResources().getString(R.string.SAR));

        // check if the food has discount or not .


        // if additions equal zero
        if (data.get(position).getProduct_additions().size() == 0) {

        }
//        holder.title.setText(mData.get(position).getTitle());
        viewHolder.mFamilyOrderSpecialAdditiveAdapter.setData(data.get(position).getProduct_additions()); // List of Strings
        viewHolder.mFamilyOrderSpecialAdditiveAdapter.setRowIndex(position);
    }


    public class ViewHolder extends ParentRecyclerViewHolder {

        private FamilyOrderSpecialAdditiveAdapter mFamilyOrderSpecialAdditiveAdapter;



        @BindView(R.id.tv_item_price)
        TextView tvItemPrice;

        @BindView(R.id.tv_item_name)
        TextView tvItemName;



        @BindView(R.id.tv_recycle)
        RecyclerView tvRecycle;



        ViewHolder(View itemView) {
            super(itemView);
            setClickableRootView(itemView);

            tvRecycle.setLayoutManager(new LinearLayoutManager(mcontext, LinearLayoutManager.VERTICAL, false));
            mFamilyOrderSpecialAdditiveAdapter = new FamilyOrderSpecialAdditiveAdapter(mcontext);
            tvRecycle.setAdapter(mFamilyOrderSpecialAdditiveAdapter);
        }

    }
}

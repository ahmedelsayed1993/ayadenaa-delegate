package com.aait.ayadenaadelegate.UI.Fragment;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.aait.ayadenaadelegate.R;
import com.aait.ayadenaadelegate.Base.BaseFragment;
import com.aait.ayadenaadelegate.Listeners.OnItemClickListener;
import com.aait.ayadenaadelegate.Listeners.PaginationAdapterCallback;
import com.aait.ayadenaadelegate.Listeners.PaginationScrollListener;
import com.aait.ayadenaadelegate.Models.OrderModel;
import com.aait.ayadenaadelegate.Models.NewOrderResponse;
import com.aait.ayadenaadelegate.Network.RetroWeb;
import com.aait.ayadenaadelegate.Network.ServiceApi;
import com.aait.ayadenaadelegate.Network.Urls;

import com.aait.ayadenaadelegate.UI.Activities.NewOrderDetailsActiity;
import com.aait.ayadenaadelegate.UI.Adapters.NewOrderAdapter;
import com.aait.ayadenaadelegate.Uitls.CommonUtil;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Mahmoud on 2/13/18.
 */

public class OnTheWayOrderFragment extends BaseFragment implements OnItemClickListener, PaginationAdapterCallback {

    LinearLayoutManager linearLayoutManager;

    NewOrderAdapter mOnTheWayOrderAdapter;

    List<OrderModel> mOrderModels = new ArrayList<>();

    private static final int PAGE_START = 1;

    private boolean isLoading = false;

    private boolean isLastPage = false;

    private int TOTAL_PAGES;

    private int currentPage = PAGE_START;

    public static OnTheWayOrderFragment newInstance() {
        Bundle args = new Bundle();
        OnTheWayOrderFragment fragment = new OnTheWayOrderFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_on_the_way_order;
    }

    @Override
    protected void initializeComponents(final View view) {
        linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mOnTheWayOrderAdapter = new NewOrderAdapter(mContext, mOrderModels);
        mOnTheWayOrderAdapter.setOnItemClickListener(this);
        mOnTheWayOrderAdapter.setOnPaginationClickListener(this);
        rvRecycle.setLayoutManager(linearLayoutManager);
        rvRecycle.setItemAnimator(new DefaultItemAnimator());
        rvRecycle.setAdapter(mOnTheWayOrderAdapter);
        rvRecycle.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }

        });

        swipeRefresh.setColorSchemeResources(R.color.orange, R.color.green, R.color.blue);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                onTheWayOrders(mSharedPrefManager.getUserData().getUser_id(),mLanguagePrefManager.getAppLanguage());
            }
        });

        onTheWayOrders(mSharedPrefManager.getUserData().getUser_id(),mLanguagePrefManager.getAppLanguage());

//        onTheWayOrders("Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEwLCJpc3MiOiJodHRwOi8vZWxzaGVyYmVueS5hcmFic2Rlc2lnbi5jb20vdjMvYXlhZGVuYS9hcGkvYXV0aC9zaWduaW4iLCJpYXQiOjE1MjI1ODIxNTIsImV4cCI6MTY3ODEwMjE1MiwibmJmIjoxNTIyNTgyMTUyLCJqdGkiOiJKNWpVZzliWVFnMFlibjJpIn0.CDEYfz7ycXk_Nuk2Lyc43FcikZifjuwc_8o8FlGekZA"
//                ,
//                mLanguagePrefManager.getAppLanguage());
    }

    @Override
    protected boolean isRecycle() {
        return true;
    }

    @Override
    public void onItemClick(final View view, final int position) {
        NewOrderDetailsActiity.startActivity((AppCompatActivity) mContext, mOrderModels.get(position));
    }

    @Override
    public void retryPageLoad() {

    }

    private void onTheWayOrders(int user_id, String lang) {
        layProgress.setVisibility(View.VISIBLE);
        layNoInternet.setVisibility(View.GONE);
        layNoItem.setVisibility(View.GONE);
        RetroWeb.getClient().create(ServiceApi.class).getOnWay(user_id, lang)
                .enqueue(new Callback<NewOrderResponse>() {
                    @Override
                    public void onResponse(Call<NewOrderResponse> call,
                            Response<NewOrderResponse> response) {
                        layProgress.setVisibility(View.GONE);
                        swipeRefresh.setRefreshing(false);
                        if (response.isSuccessful()) {
                            if (response.body().getData().size() == 0) {
                                layNoItem.setVisibility(View.VISIBLE);
                                layNoInternet.setVisibility(View.GONE);
                                tvNoContent.setText(R.string.no_data);
                            } else {
                                mOnTheWayOrderAdapter.updateAll(response.body().getData());
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<NewOrderResponse> call, Throwable t) {
                        CommonUtil.handleException(mContext, t);
                        t.printStackTrace();
                        layNoInternet.setVisibility(View.VISIBLE);
                        layNoItem.setVisibility(View.GONE);
                        layProgress.setVisibility(View.GONE);
                        swipeRefresh.setRefreshing(false);
                    }
                });
    }
}
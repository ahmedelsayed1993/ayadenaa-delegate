package com.aait.ayadenaadelegate.UI.Activities;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aait.ayadenaadelegate.Base.ParentActivity;
import com.aait.ayadenaadelegate.Listeners.OnItemClickListener;
import com.aait.ayadenaadelegate.Listeners.PaginationAdapterCallback;
import com.aait.ayadenaadelegate.Listeners.PaginationScrollListener;
import com.aait.ayadenaadelegate.Models.NewOrderResponse;
import com.aait.ayadenaadelegate.Models.NotificationModel;
import com.aait.ayadenaadelegate.Models.NotificationResponse;
import com.aait.ayadenaadelegate.Models.OrderModel;
import com.aait.ayadenaadelegate.Network.RetroWeb;
import com.aait.ayadenaadelegate.Network.ServiceApi;
import com.aait.ayadenaadelegate.R;
import com.aait.ayadenaadelegate.UI.Adapters.NewOrderAdapter;
import com.aait.ayadenaadelegate.UI.Adapters.NotificationAdapter;
import com.aait.ayadenaadelegate.Uitls.CommonUtil;

import java.util.ArrayList;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CurrentOrdersActivity extends ParentActivity implements OnItemClickListener ,PaginationAdapterCallback {

    LinearLayoutManager linearLayoutManager;

    NewOrderAdapter mOrderAdapter;
    @BindView(R.id.swipe_refresh)
            SwipeRefreshLayout swipeRefresh;
    @BindView(R.id.lay_no_internet)
    RelativeLayout layNoInternet;
    @BindView(R.id.lay_no_item)
      RelativeLayout layNoItem;
    @BindView(R.id.lay_progress)
            RelativeLayout layProgress;
    @BindView(R.id.rv_recycle)
    RecyclerView rvRecycle;
    @BindView(R.id.tv_no_content)
    TextView tvNoContent;

    ArrayList<OrderModel> mOrdermodels = new ArrayList<>();

    private static final int PAGE_START = 1;

    private boolean isLoading = false;

    private boolean isLastPage = false;

    private int TOTAL_PAGES;

    private int currentPage = PAGE_START;
    @Override
    protected void initializeComponents() {
        setToolbarTitle(getString(R.string.processed_orders));
        linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        rvRecycle.setLayoutManager(linearLayoutManager);
        mOrderAdapter = new NewOrderAdapter(mContext, mOrdermodels);
        mOrderAdapter.setOnItemClickListener(this);
        mOrderAdapter.setOnPaginationClickListener(this);
        rvRecycle.setAdapter(mOrderAdapter);
        rvRecycle.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }

        });

        swipeRefresh.setColorSchemeResources(R.color.orange, R.color.green, R.color.blue);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                getnotifications();
            }
        });

        getnotifications();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_recycle;
    }

    @Override
    protected boolean isEnableToolbar() {
        return true;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return true;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }

    @Override
    public void onItemClick(View view, int position) {
        NewOrderDetailsActiity.startActivity((AppCompatActivity)mContext,mOrdermodels.get(position));

    }


    @Override
    public void retryPageLoad() {

    }
    private void getnotifications() {
        layProgress.setVisibility(View.VISIBLE);
        layNoInternet.setVisibility(View.GONE);
        layNoItem.setVisibility(View.GONE);
        RetroWeb.getClient().create(ServiceApi.class).getCurrent(mSharedPrefManager.getUserData().getUser_id(), mLanguagePrefManager.getAppLanguage())
                .enqueue(new Callback<NewOrderResponse>() {
                    @Override
                    public void onResponse(Call<NewOrderResponse> call,
                                           Response<NewOrderResponse> response) {
                        layProgress.setVisibility(View.GONE);
                        swipeRefresh.setRefreshing(false);
                        if (response.isSuccessful()) {
                            if (response.body().getData().size() == 0) {
                                layNoItem.setVisibility(View.VISIBLE);
                                layNoInternet.setVisibility(View.GONE);
                                tvNoContent.setText(R.string.no_data);
                            } else {
                                mOrderAdapter.updateAll(response.body().getData());
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<NewOrderResponse> call, Throwable t) {
                        CommonUtil.handleException(mContext, t);
                        t.printStackTrace();
                        layNoInternet.setVisibility(View.VISIBLE);
                        layNoItem.setVisibility(View.GONE);
                        layProgress.setVisibility(View.GONE);
                        swipeRefresh.setRefreshing(false);
                    }
                });
    }
}

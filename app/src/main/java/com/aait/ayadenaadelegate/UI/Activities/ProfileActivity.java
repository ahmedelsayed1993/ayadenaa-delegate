package com.aait.ayadenaadelegate.UI.Activities;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.aait.ayadenaadelegate.App.Constant;
import com.aait.ayadenaadelegate.Models.UserModel;
import com.aait.ayadenaadelegate.R;
import com.aait.ayadenaadelegate.Base.ParentActivity;
import com.aait.ayadenaadelegate.Listeners.OnItemClickListener;
import com.aait.ayadenaadelegate.Models.ListModel;
import com.aait.ayadenaadelegate.Models.ListModelResponse;
import com.aait.ayadenaadelegate.Models.LoginResponse;
import com.aait.ayadenaadelegate.Network.RetroWeb;
import com.aait.ayadenaadelegate.Network.ServiceApi;
import com.aait.ayadenaadelegate.Network.Urls;
import com.aait.ayadenaadelegate.UI.Views.ListDialog;
import com.aait.ayadenaadelegate.Uitls.CommonUtil;
import com.aait.ayadenaadelegate.Uitls.PermissionUtils;
import com.aait.ayadenaadelegate.Uitls.ProgressRequestBody;
import com.aait.ayadenaadelegate.Uitls.ValidationUtils;
import com.bumptech.glide.Glide;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import gun0912.tedbottompicker.TedBottomPicker;
import gun0912.tedbottompicker.TedBottomPicker.Builder;
import gun0912.tedbottompicker.TedBottomPicker.OnMultiImageSelectedListener;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.aait.ayadenaadelegate.App.Constant.RequestPermission.REQUEST_IMAGES;

/**
 * Created by Mahmoud on 2/6/18.
 */

public class ProfileActivity extends ParentActivity
        implements OnItemClickListener, ProgressRequestBody.UploadCallbacks {

    @BindView(R.id.lay_splash)
    LinearLayout laySplash;

    @BindView(R.id.civ_profile_pic)
    CircleImageView civProfilePic;

    @BindView(R.id.til_name)
    TextInputLayout tilName;

    @BindView(R.id.et_name)
    TextInputEditText etName;

    @BindView(R.id.til_email)
    TextInputLayout tilEmail;

    @BindView(R.id.et_email)
    TextInputEditText etEmail;

    @BindView(R.id.til_phone)
    TextInputLayout tilPhone;

    @BindView(R.id.et_phone)
    TextInputEditText etPhone;

    @BindView(R.id.til_location)
    TextInputLayout tilLocation;

    @BindView(R.id.et_location)
    TextInputEditText etLocation;

    @BindView(R.id.til_password)
    TextInputLayout tilPassword;

    @BindView(R.id.et_password)
    TextInputEditText etPassword;

    @BindView(R.id.til_id)
    TextInputLayout tilId;

    @BindView(R.id.et_id)
    TextInputEditText etId;

    @BindView(R.id.til_nationality)
    TextInputLayout tilNationality;

    @BindView(R.id.et_nationality)
    TextInputEditText etNationality;

    @BindView(R.id.til_location_address)
    TextInputLayout tilLocationAddress;

    @BindView(R.id.et_location_address)
    TextInputEditText etLocationAddress;

    ArrayList<ListModel> mListModels;

    String mAdresse, mLang, mLat = null;

    ListDialog mListDialog;

    ListModel cityModel;

    ListModel NationalityModel;


    // select image from callery
    ArrayList<Uri> ImageList = new ArrayList<>();

    String ImageBasePath = null;

    int NationalityOrCity = 0;
    private static final int REQUEST_CODE = 4;
    private static final int REQUEST_CODE_MATISSE = 5;
    private String type = null;


    public static void startActivity(AppCompatActivity mAppCompatActivity) {
        Intent mIntent = new Intent(mAppCompatActivity, ProfileActivity.class);
        mAppCompatActivity.startActivity(mIntent);
    }


    @Override
    protected void initializeComponents() {
        setToolbarTitle(getString(R.string.profile));
      getProfile();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_profile;
    }

    @Override
    protected boolean isEnableToolbar() {
        return true;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return true;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }

    @Override
    public void onProgressUpdate(final int percentage) {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }


    @OnClick(R.id.btn_save_changes)
    void onSaveChangeClick() {
        if (editValidation()) {
            if (ImageBasePath == null) {
              updatewithPass();
            } else {
                 updatewithImageAndPass(ImageBasePath);
            }
        }
    }

    @Override
    public void onItemClick(final View view, final int position) {
        mListDialog.dismiss();
        if (NationalityOrCity == 0) {
            cityModel = mListModels.get(position);
            etLocation.setText(cityModel.getName());
        } else {
            NationalityModel = mListModels.get(position);
            etNationality.setText(NationalityModel.getName());
        }
        CommonUtil.PrintLogE("City id : " + cityModel.getId());
    }

    @OnClick(R.id.et_password)
    void onChangePasswordClick() {
        ChangePasswordActivity.startActivity((AppCompatActivity) mContext);
    }

    @OnClick(R.id.et_location)
    void onCityClick() {
        NationalityOrCity = 0;
        getCities(mLanguagePrefManager.getAppLanguage());
    }

    @OnClick(R.id.et_nationality)
    void onNationalityClick() {
        NationalityOrCity = 1;
        getNationalitles(mLanguagePrefManager.getAppLanguage());
    }

    @OnClick(R.id.et_location_address)
    void onLocationAdreesClick() {
        MapDetectLocationActivity.startActivityForResult((AppCompatActivity) mContext);
    }

    @OnClick(R.id.civ_profile_pic)
    void onImageClick() {
//        getPickImageWithPermission();
        getFile();

    }
    private void getFile() {
        if (ActivityCompat.checkSelfPermission(ProfileActivity.this, android.Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_CODE);
        } else {
            attach();
        }
    }
    private void attach() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent,
                "Select file to upload "), REQUEST_CODE_MATISSE);

    }

    void setUserData(UserModel userData) {
        cityModel = new ListModel(userData.getCity_id(),userData.getCity_name()) ;
        NationalityModel = new ListModel(userData.getNationality_id(),userData.getNationality_name());
        mLat = userData.getLat();
        mLang = userData.getLng();

        etName.setText(userData.getName());
        etEmail.setText(userData.getEmail());
        etPhone.setText(userData.getPhone());
        etLocation.setText(userData.getCity_name());
        etId.setText(userData.getCivil_number());
        etNationality.setText(userData.getNationality_name());

        Glide.with(mContext).load(userData.getAvatar()).asBitmap().into(civProfilePic);

    }
    private void getProfile(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getProfile(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getUser_id()).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        setUserData(response.body().getData());
                    }
                    else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();


            }
        });
    }

    boolean editValidation() {
        if (!ValidationUtils.checkError(etName, tilName, getString(R.string.fill_empty))) {
            return false;
        } else if (!ValidationUtils.checkError(etPhone, tilPhone, getString(R.string.fill_empty))) {
            return false;
        }else if (etPhone.getText().toString().length()<9){
            CommonUtil.makeToast(mContext,getString(R.string.phone_validation));
            return false;
        } else if (!ValidationUtils.checkError(etId, tilId, getString(R.string.fill_empty))) {
            return false;
        } else if (!ValidationUtils.checkError(etEmail, tilEmail, getString(R.string.fill_empty))) {
            return false;
        } else if (!ValidationUtils.validateEmail(etEmail, tilEmail,getString(R.string.enter_your_email),getString(R.string.Email_is_not_in_correct_format))) {
            return false;
        } else if (!ValidationUtils.checkError(etNationality, tilNationality, getString(R.string.fill_empty))) {
            return false;
        } else if (!ValidationUtils.checkError(etLocation, tilLocation, getString(R.string.fill_empty))) {
            return false;
        }
        return true;
    }


    public void getPickImageWithPermission() {
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!PermissionUtils.hasPermissions(mContext, PermissionUtils.IMAGE_PERMISSIONS)) {
                CommonUtil.PrintLogE("Permission not granted");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(PermissionUtils.IMAGE_PERMISSIONS,
                            REQUEST_IMAGES);
                }
            } else {
                pickMultiImages();
                CommonUtil.PrintLogE("Permission is granted before");
            }
        } else {
            CommonUtil.PrintLogE("SDK minimum than 23");
            pickMultiImages();
        }
    }

    void pickMultiImages() {
        TedBottomPicker bottomSheetDialogFragment = new Builder(mContext)
                .setOnMultiImageSelectedListener(new OnMultiImageSelectedListener() {
                    @Override
                    public void onImagesSelected(ArrayList<Uri> uriList) {
                        ImageList = uriList;
                        ImageBasePath = ImageList.get(0).getPath();
                        civProfilePic.setImageURI(Uri.parse(ImageBasePath));

                    }
                })
                .setTitle(R.string.avatar)
                .setSelectMaxCount(1)
                .setSelectMinCount(1)
                .setPeekHeight(2600)
                .showTitle(false)
                .setCompleteButtonText(R.string.choose)
                .setEmptySelectionText(R.string.no_item_selected_yet)
                .create();
        bottomSheetDialogFragment.show(getSupportFragmentManager());
    }




    private void getCities(String lang) {
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getCities(lang)
                .enqueue(new Callback<ListModelResponse>() {
                    @Override
                    public void onResponse(Call<ListModelResponse> call,
                            Response<ListModelResponse> response) {
                        hideProgressDialog();
                        if (response.isSuccessful()) {
                            if (response.body().getStatus()==1) {
                                mListModels = response.body().getData();
                                mListDialog = new ListDialog(mContext, ProfileActivity.this,
                                        mListModels,
                                        getString(R.string.city));
                                mListDialog.show();
                            } else {
                                CommonUtil.makeToast(mContext, response.body().getMsg());
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ListModelResponse> call, Throwable t) {
                        CommonUtil.handleException(mContext, t);
                        t.printStackTrace();
                        hideProgressDialog();
                    }
                });
    }



    private void getNationalitles(String lang) {
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getNations(lang)
                .enqueue(new Callback<ListModelResponse>() {
                    @Override
                    public void onResponse(Call<ListModelResponse> call,
                            Response<ListModelResponse> response) {
                        hideProgressDialog();
                        if (response.isSuccessful()) {
                            if (response.body().getStatus()==1) {
                                mListModels = response.body().getData();
                                mListDialog = new ListDialog(mContext, ProfileActivity.this,
                                        mListModels,
                                        getString(R.string.nationality));
                                mListDialog.show();
                            } else {
                                CommonUtil.makeToast(mContext, response.body().getMsg());
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ListModelResponse> call, Throwable t) {
                        CommonUtil.handleException(mContext, t);
                        t.printStackTrace();
                        hideProgressDialog();
                    }
                });
    }

    private void updatewithPass(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).clientUpdatewithoutImage(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getUser_id()+"",
                etName.getText().toString(),etEmail.getText().toString(),etPhone.getText().toString(),cityModel.getId()+"",mLat,mLang,NationalityModel.getId(),etId.getText().toString()).
                enqueue(new Callback<LoginResponse>() {
                    @Override
                    public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                        hideProgressDialog();
                        if (response.isSuccessful()) {
                            if (response.body().getStatus() == 1) {
                                mSharedPrefManager.setUserData(response.body().getData());
                                setUserData(response.body().getData());
                                CommonUtil.makeToast(mContext, getString(R.string.profile_update_succ));
                            } else if (response.body().getStatus()==0){
                                CommonUtil.makeToast(mContext, response.body().getMsg());
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<LoginResponse> call, Throwable t) {
                        CommonUtil.handleException(mContext, t);
                        t.printStackTrace();
                        hideProgressDialog();
                    }
                });
    }


    private void updatewithImageAndPass(String pathFromImage){
        showProgressDialog(getString(R.string.please_wait));
        MultipartBody.Part filePart = null;
        File ImageFile = new File(pathFromImage);
        ProgressRequestBody fileBody = new ProgressRequestBody(ImageFile, ProfileActivity.this);
        filePart = MultipartBody.Part.createFormData("avatar", ImageFile.getName(), fileBody);
        RetroWeb.getClient().create(ServiceApi.class).clientUpdate(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getUser_id()+"",
                etName.getText().toString(),etEmail.getText().toString(),etPhone.getText().toString(),cityModel.getId()+""
                ,mLat,mLang,NationalityModel.getId(),etId.getText().toString(),filePart).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == 1) {
                        mSharedPrefManager.setUserData(response.body().getData());
                        setUserData(response.body().getData());
                        CommonUtil.makeToast(mContext, getString(R.string.profile_update_succ));
                    } else if (response.body().getStatus()==0){
                        CommonUtil.makeToast(mContext, response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                CommonUtil.handleException(mContext, t);
                t.printStackTrace();
                hideProgressDialog();
            }
        });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data != null) {
            if (requestCode == Constant.RequestCode.GET_LOCATION) {
                if (resultCode == RESULT_OK) {
                    mAdresse = data.getStringExtra(Constant.LocationConstant.LOCATION);
                    mLang = data.getStringExtra(Constant.LocationConstant.LNG);
                    mLat = data.getStringExtra(Constant.LocationConstant.LAT);
                    CommonUtil.PrintLogE("Lat : " + mLat + " Lng : " + mLang + " Address : " + mAdresse);
                    etLocationAddress.setText(mAdresse);
                }
            }else   if (requestCode == REQUEST_CODE_MATISSE) {
                if (resultCode == RESULT_OK) {
                    Uri uri = data.getData();
                    Log.e("path", uri.toString());
                    String path = null;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        path = CommonUtil.getPath(this, uri);
                        Log.e("path", path);

                        type = getContentResolver().getType(uri);
                        Log.e("type",type);
                        if ((type.startsWith("image"))){
                            ImageBasePath = CommonUtil.getPath(ProfileActivity.this,uri);

                            civProfilePic.setImageURI(Uri.parse(ImageBasePath));

                        }else {

                        }
                    }

                }

            }
        }
    }

}

package com.aait.ayadenaadelegate.UI.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aait.ayadenaadelegate.R;
import com.aait.ayadenaadelegate.Base.ParentRecyclerAdapter;
import com.aait.ayadenaadelegate.Base.ParentRecyclerViewHolder;
import com.aait.ayadenaadelegate.Models.OrderProductAdditionModel;

import java.util.List;

import butterknife.BindView;

/**
 * Created by Mahmoud ibrahim on 12/2/2018.
 */

public class FamilyOrderSpecialAdditiveAdapter extends ParentRecyclerAdapter<OrderProductAdditionModel> {

    private int mRowIndex = -1;

    public void setData(List<OrderProductAdditionModel> modelData) {
        if (data != modelData) {
            data = modelData;
            notifyDataSetChanged();
        }
    }

    public void setRowIndex(int index) {
        mRowIndex = index;
    }

    public FamilyOrderSpecialAdditiveAdapter(final Context mContext) {
        super(mContext);
    }

    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        View itemView = LayoutInflater.from(mcontext)
                .inflate(R.layout.recycle_family_order_special_adictive, parent, false);
        ViewHolder holder = new ViewHolder(itemView);
        holder.setOnItemClickListener(itemClickListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ParentRecyclerViewHolder holder, final int position) {
        final ViewHolder viewHolder = (ViewHolder) holder;
        OrderProductAdditionModel specialAdditiveModel = data.get(position);
        viewHolder.tvAdditionPrice
                .setText(specialAdditiveModel.getAddition_price() + " " + mcontext.getResources().getString(R.string.SAR));
        viewHolder.tvAdditionName.setText(specialAdditiveModel.getAddition_name());

    }


    public class ViewHolder extends ParentRecyclerViewHolder {

        @BindView(R.id.tv_addition_price)
        TextView tvAdditionPrice;

        @BindView(R.id.tv_addition_name)
        TextView tvAdditionName;



        ViewHolder(View itemView) {
            super(itemView);
            setClickableRootView(itemView);
        }

    }
}

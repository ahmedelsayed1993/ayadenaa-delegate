package com.aait.ayadenaadelegate.UI.Activities;

import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.aait.ayadenaadelegate.R;
import com.aait.ayadenaadelegate.Base.ParentActivity;
import com.aait.ayadenaadelegate.Listeners.OnItemClickListener;
import com.aait.ayadenaadelegate.Listeners.PaginationAdapterCallback;
import com.aait.ayadenaadelegate.Models.OrderModel;
import com.aait.ayadenaadelegate.Models.NewOrderResponse;
import com.aait.ayadenaadelegate.Network.RetroWeb;
import com.aait.ayadenaadelegate.Network.ServiceApi;
import com.aait.ayadenaadelegate.Network.Urls;
import com.aait.ayadenaadelegate.UI.Adapters.NewOrderAdapter;
import com.aait.ayadenaadelegate.Uitls.CommonUtil;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Mahmoud on 3/10/18.
 */

public class PreviousOrders extends ParentActivity implements OnItemClickListener, PaginationAdapterCallback {

    LinearLayoutManager linearLayoutManager;

    NewOrderAdapter mPreviousOrderAdapter;

    List<OrderModel> mOrderModels = new ArrayList<>();

    public static void startActivity(AppCompatActivity mAppCompatActivity) {
        Intent mIntent = new Intent(mAppCompatActivity, PreviousOrders.class);
        mAppCompatActivity.startActivity(mIntent);
    }

    @Override
    protected void initializeComponents() {
        setToolbarTitle(getResources().getString(R.string.previous_orders));

        mOrderModels.add(new OrderModel());
        mOrderModels.add(new OrderModel());
        mOrderModels.add(new OrderModel());

        linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mPreviousOrderAdapter = new NewOrderAdapter(mContext, mOrderModels);
        mPreviousOrderAdapter.setOnItemClickListener(this);
        rvRecycle.setLayoutManager(linearLayoutManager);
        rvRecycle.setItemAnimator(new DefaultItemAnimator());
        rvRecycle.setAdapter(mPreviousOrderAdapter);

        swipeRefresh.setColorSchemeResources(R.color.orange, R.color.green, R.color.blue);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getPreviousOrder(mSharedPrefManager.getUserData().getUser_id(),mLanguagePrefManager.getAppLanguage());
            }
        });

        getPreviousOrder(mSharedPrefManager.getUserData().getUser_id(),mLanguagePrefManager.getAppLanguage());

//        getPreviousOrder(
//                "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEwLCJpc3MiOiJodHRwOi8vZWxzaGVyYmVueS5hcmFic2Rlc2lnbi5jb20vdjMvYXlhZGVuYS9hcGkvYXV0aC9zaWduaW4iLCJpYXQiOjE1MjI1ODIxNTIsImV4cCI6MTY3ODEwMjE1MiwibmJmIjoxNTIyNTgyMTUyLCJqdGkiOiJKNWpVZzliWVFnMFlibjJpIn0.CDEYfz7ycXk_Nuk2Lyc43FcikZifjuwc_8o8FlGekZA",
//                mLanguagePrefManager.getAppLanguage());
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_recycle;
    }

    @Override
    protected boolean isEnableToolbar() {
        return true;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return true;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return true;
    }

    @Override
    public void onItemClick(final View view, final int position) {
        NewOrderDetailsActiity.startActivity((AppCompatActivity) mContext, mOrderModels.get(position));
    }

    @Override
    public void retryPageLoad() {

    }

    private void getPreviousOrder(int user_id, String lang) {
        layProgress.setVisibility(View.VISIBLE);
        layNoInternet.setVisibility(View.GONE);
        layNoItem.setVisibility(View.GONE);
        RetroWeb.getClient().create(ServiceApi.class).getFinishedOrders( lang,user_id)
                .enqueue(new Callback<NewOrderResponse>() {
                    @Override
                    public void onResponse(Call<NewOrderResponse> call,
                            Response<NewOrderResponse> response) {
                        layProgress.setVisibility(View.GONE);
                        swipeRefresh.setRefreshing(false);
                        if (response.isSuccessful()) {
                            if (response.body().getData().size() == 0) {
                                layNoItem.setVisibility(View.VISIBLE);
                                layNoInternet.setVisibility(View.GONE);
                                tvNoContent.setText(R.string.no_data);
                            } else {
                                mPreviousOrderAdapter.updateAll(response.body().getData());
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<NewOrderResponse> call, Throwable t) {
                        CommonUtil.handleException(mContext, t);
                        t.printStackTrace();
                        layNoInternet.setVisibility(View.VISIBLE);
                        layNoItem.setVisibility(View.GONE);
                        layProgress.setVisibility(View.GONE);
                        swipeRefresh.setRefreshing(false);
                    }
                });
    }
}

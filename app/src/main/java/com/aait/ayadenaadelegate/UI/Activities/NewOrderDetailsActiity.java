package com.aait.ayadenaadelegate.UI.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.ayadenaadelegate.App.Constant;
import com.aait.ayadenaadelegate.R;
import com.aait.ayadenaadelegate.Base.ParentActivity;
import com.aait.ayadenaadelegate.Models.BaseResponse;
import com.aait.ayadenaadelegate.Models.OrderProductModel;
import com.aait.ayadenaadelegate.Models.OrderModel;
import com.aait.ayadenaadelegate.Models.OrderModel;
import com.aait.ayadenaadelegate.Network.RetroWeb;
import com.aait.ayadenaadelegate.Network.ServiceApi;
import com.aait.ayadenaadelegate.Network.Urls;

import com.aait.ayadenaadelegate.UI.Adapters.OrderDetailsAdapter;
import com.aait.ayadenaadelegate.Uitls.CommonUtil;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Mahmoud on 3/10/18.
 */

public class NewOrderDetailsActiity extends ParentActivity {

    @BindView(R.id.tv_order_time)
    TextView tvOrderTime;



    @BindView(R.id.tv_order_number)
    TextView tvOrderNumber;
    @BindView(R.id.tv_delevery_coast)
    TextView tv_delevery_coast;

    @BindView(R.id.iv_client_image)
    CircleImageView ivClientImage;

    @BindView(R.id.tv_client_name)
    TextView tvClientName;

    @BindView(R.id.tv_client_city)
    TextView tvClientCity;

    @BindView(R.id.tv_client_mobile)
    TextView tvClientMobile;

    @BindView(R.id.iv_family_image)
    CircleImageView ivFamilyImage;

    @BindView(R.id.tv_family_name)
    TextView tvFamilyName;

    @BindView(R.id.tv_family_city)
    TextView tvFamilyCity;

    @BindView(R.id.tv_family_mobile)
    TextView tvFamilyMobile;

    @BindView(R.id.rv_recycle)
    RecyclerView rvRecycle;
    @BindView(R.id.iv_step_two)
    ImageView iv_step_two;
    @BindView(R.id.tv_step_two)
    TextView tv_step_two;
    @BindView(R.id.iv_step_three)
    ImageView iv_step_three;
    @BindView(R.id.tv_step_three)
    TextView tv_step_three;
    @BindView(R.id.iv_step_four)
    ImageView iv_step_four;
    @BindView(R.id.tv_step_four)
    TextView tv_step_four;
   @BindView(R.id.iv_order_status)
   ImageView iv_order_status;

    @BindView(R.id.tv_total_coast)
    TextView tv_total_coast;
    @BindView(R.id.btn_accept)
    Button btn_accept;
    @BindView(R.id.btn_accept1)
            Button btn_accept1;
    @BindView(R.id.btn_accept2)
            Button btn_accept2;


    LinearLayoutManager linearLayoutManager;

    OrderDetailsAdapter mFamilyOrderDetailsAdapter;

    List<OrderProductModel> mFamilyOrderModels = new ArrayList<>();

    OrderModel mOrderModel;

    public static void startActivity(AppCompatActivity mAppCompatActivity, OrderModel orderModel) {
        Intent mIntent = new Intent(mAppCompatActivity, NewOrderDetailsActiity.class);
        mIntent.putExtra(Constant.BundleData.ORDER, orderModel);
        mAppCompatActivity.startActivity(mIntent);
    }

    void getBundleData() {
        mOrderModel = (OrderModel) getIntent().getSerializableExtra(Constant.BundleData.ORDER);
        setOrderData(mOrderModel);
    }

    @Override
    protected void initializeComponents() {
        setToolbarTitle(getString(R.string.order_details));


        linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mFamilyOrderDetailsAdapter = new OrderDetailsAdapter(mContext, mFamilyOrderModels,
                R.layout.recycle_family_order_details);
        rvRecycle.setLayoutManager(linearLayoutManager);
        rvRecycle.setAdapter(mFamilyOrderDetailsAdapter);
        getBundleData();



//        getOrdersDetails(
//                "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEwLCJpc3MiOiJodHRwOi8vZWxzaGVyYmVueS5hcmFic2Rlc2lnbi5jb20vdjMvYXlhZGVuYS9hcGkvYXV0aC9zaWduaW4iLCJpYXQiOjE1MjI1ODIxNTIsImV4cCI6MTY3ODEwMjE1MiwibmJmIjoxNTIyNTgyMTUyLCJqdGkiOiJKNWpVZzliWVFnMFlibjJpIn0.CDEYfz7ycXk_Nuk2Lyc43FcikZifjuwc_8o8FlGekZA",
//                mLanguagePrefManager.getAppLanguage(), mOrderModel.getId());
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_new_order_details;
    }

    @Override
    protected boolean isEnableToolbar() {
        return true;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return true;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }

    void setOrderData(OrderModel orderData) {

        Glide.with(mContext).load(orderData.getOrder_user_avatar()).asBitmap()
                .into(ivClientImage);
        Glide.with(mContext).load(orderData.getOrder_provider_avatar()).asBitmap()
                .into(ivFamilyImage);
        tvOrderTime
                .setText(orderData.getOrder_created_at() + " ");
        tvOrderNumber
                .setText(orderData.getOrder_id() + "");

        tvClientName.setText(orderData.getOrder_user_name());
        tvFamilyName.setText(orderData.getOrder_provider_name());

        tvClientCity.setText(orderData.getOrder_user_city());
        tvFamilyCity.setText(orderData.getOrder_provider_city());

        tvClientMobile.setText(orderData.getOrder_user_phone());
        tvFamilyMobile.setText(orderData.getOrder_provider_phone());
        tv_delevery_coast.setText(":  "+orderData.getDelegate_price()+mContext.getResources().getString(R.string.SAR));
        mFamilyOrderDetailsAdapter.updateAll(orderData.getOrder_products());

        tv_total_coast.setText(orderData.getTotal_price()+mContext.getResources().getString(R.string.SAR));
        if (orderData.getOrder_status()>3){
            btn_accept.setVisibility(View.GONE);
            iv_step_two.setImageResource(R.mipmap.step_two);
            tv_step_two.setTextColor(mContext.getResources().getColor(R.color.order_step_2_color));
            iv_order_status.setImageResource(R.mipmap.order_two);

        }
        if (orderData.getOrder_status()>6){
            btn_accept.setVisibility(View.GONE);
            iv_step_three.setImageResource(R.mipmap.step_three);
            tv_step_three.setTextColor(mContext.getResources().getColor(R.color.order_step_3_color));
            iv_order_status.setImageResource(R.mipmap.order_three);

        }
        if (orderData.getOrder_status()>7){
            btn_accept.setVisibility(View.GONE);
            iv_step_four.setImageResource(R.mipmap.step_four);
            tv_step_four.setTextColor(mContext.getResources().getColor(R.color.order_step_4_color));
            iv_order_status.setImageResource(R.mipmap.order_four);
        }
        if (orderData.getOrder_status()==6){
            btn_accept1.setVisibility(View.VISIBLE);
        }
        if (orderData.getOrder_status()==7){
            btn_accept2.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.btn_accept)
    void onBtnAcceptClick() {

        change(mOrderModel.getOrder_id());
    }
    @OnClick(R.id.btn_accept1)
    void onOnWayClick(){
        change1(mOrderModel.getOrder_id());
    }
    @OnClick(R.id.btn_accept2)
    void onReceive(){
        change2(mOrderModel.getOrder_id());
    }

    private void change(int order_id){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).changeStatus(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getUser_id(),order_id,3).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        CommonUtil.makeToast(mContext,getString(R.string.accept_order));
                        startActivity(new Intent(mContext, MainActivity.class));
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    private void change1(int order_id){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).changeStatus(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getUser_id(),order_id,6).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        CommonUtil.makeToast(mContext,getString(R.string.orders));
                        startActivity(new Intent(mContext, MainActivity.class));
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    private void change2(int order_id){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).changeStatus(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getUser_id(),order_id,7).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        CommonUtil.makeToast(mContext,getString(R.string.client_receive_order));
                        startActivity(new Intent(mContext, MainActivity.class));
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
}

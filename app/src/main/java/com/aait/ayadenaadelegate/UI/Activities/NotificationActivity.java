package com.aait.ayadenaadelegate.UI.Activities;

import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.aait.ayadenaadelegate.Base.ParentActivity;
import com.aait.ayadenaadelegate.Listeners.OnItemClickListener;
import com.aait.ayadenaadelegate.Listeners.PaginationAdapterCallback;
import com.aait.ayadenaadelegate.Listeners.PaginationScrollListener;
import com.aait.ayadenaadelegate.Models.NotificationModel;
import com.aait.ayadenaadelegate.Models.NotificationResponse;
import com.aait.ayadenaadelegate.Network.RetroWeb;
import com.aait.ayadenaadelegate.Network.ServiceApi;
import com.aait.ayadenaadelegate.R;
import com.aait.ayadenaadelegate.UI.Adapters.NotificationAdapter;
import com.aait.ayadenaadelegate.Uitls.CommonUtil;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 *created by ahmed el_sayed 1/11/2018
 */
public class NotificationActivity extends ParentActivity implements OnItemClickListener,PaginationAdapterCallback {

    LinearLayoutManager linearLayoutManager;

    NotificationAdapter mNotificationAdapter;

    ArrayList<NotificationModel> mNotificationList = new ArrayList<>();

    private static final int PAGE_START = 1;

    private boolean isLoading = false;

    private boolean isLastPage = false;

    private int TOTAL_PAGES;

    private int currentPage = PAGE_START;

    public static void startActivity(AppCompatActivity mAppCompatActivity) {
        Intent mIntent = new Intent(mAppCompatActivity, NotificationActivity.class);
        mAppCompatActivity.startActivity(mIntent);
    }


    @Override
    protected void initializeComponents() {
        setToolbarTitle(getString(R.string.notification));
        linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        rvRecycle.setLayoutManager(linearLayoutManager);
        mNotificationAdapter = new NotificationAdapter(mContext, mNotificationList);
        mNotificationAdapter.setOnItemClickListener(this);
        mNotificationAdapter.setOnPaginationClickListener(this);
        rvRecycle.setAdapter(mNotificationAdapter);
        rvRecycle.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }

        });

        swipeRefresh.setColorSchemeResources(R.color.orange, R.color.green, R.color.blue);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getnotifications();
            }
        });

        getnotifications();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_recycle;
    }

    @Override
    protected boolean isEnableToolbar() {
        return true;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return true;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return true;
    }

    @Override
    public void retryPageLoad() {

    }



    @Override
    public void onItemClick(View view, int position) {


    }
    private void getnotifications() {
        layProgress.setVisibility(View.VISIBLE);
        layNoInternet.setVisibility(View.GONE);
        layNoItem.setVisibility(View.GONE);
        RetroWeb.getClient().create(ServiceApi.class).getNotifications(mLanguagePrefManager.getAppLanguage(), mSharedPrefManager.getUserData().getUser_id())
                .enqueue(new Callback<NotificationResponse>() {
                    @Override
                    public void onResponse(Call<NotificationResponse> call,
                                           Response<NotificationResponse> response) {
                        layProgress.setVisibility(View.GONE);
                        swipeRefresh.setRefreshing(false);
                        if (response.isSuccessful()) {
                            if (response.body().getData().size() == 0) {
                                layNoItem.setVisibility(View.VISIBLE);
                                layNoInternet.setVisibility(View.GONE);
                                tvNoContent.setText(R.string.no_data);
                            } else {
                                mNotificationAdapter.updateAll(response.body().getData());
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<NotificationResponse> call, Throwable t) {
                        CommonUtil.handleException(mContext, t);
                        t.printStackTrace();
                        layNoInternet.setVisibility(View.VISIBLE);
                        layNoItem.setVisibility(View.GONE);
                        layProgress.setVisibility(View.GONE);
                        swipeRefresh.setRefreshing(false);
                    }
                });
    }
}

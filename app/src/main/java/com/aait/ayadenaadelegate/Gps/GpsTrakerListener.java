package com.aait.ayadenaadelegate.Gps;

public interface GpsTrakerListener {
    void onTrackerSuccess(Double lat, Double log);

    void onStartTracker();
}

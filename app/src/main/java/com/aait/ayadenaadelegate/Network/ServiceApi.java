package com.aait.ayadenaadelegate.Network;

import com.aait.ayadenaadelegate.Models.BaseResponse;

import com.aait.ayadenaadelegate.Models.ConfirmCodeModel;
import com.aait.ayadenaadelegate.Models.ContactUsResponse;
import com.aait.ayadenaadelegate.Models.ForgotPasswordResponse;
import com.aait.ayadenaadelegate.Models.ListModelResponse;
import com.aait.ayadenaadelegate.Models.LoginResponse;
import com.aait.ayadenaadelegate.Models.MyDebtRespose;
import com.aait.ayadenaadelegate.Models.NewOrderResponse;
import com.aait.ayadenaadelegate.Models.NewPasswordResponse;
import com.aait.ayadenaadelegate.Models.NotificationResponse;
import com.aait.ayadenaadelegate.Models.RegisterResponse;
import com.aait.ayadenaadelegate.Models.TermsResponse;


import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface ServiceApi {
//    @GET(Urls.Areas)
//    Call<ListModelResponse> getAreas(
//            @Query("lang") String lang
//    );
    @POST(Urls.Cities)
    Call<ListModelResponse> getCities(
            @Query("lang") String lang
    );
    @GET(Urls.Nations)
    Call<ListModelResponse> getNations(
            @Query("lang") String lang
    );
//    @GET(Urls.Categories)
//    Call<ListModelResponse> getCategories(
//            @Query("lang") String lang
//    );
//    @GET(Urls.Categories)
//    Call<CategoriesResponse> getCategory(
//            @Query("lang") String lang
//    );
    @GET(Urls.Terms)
    Call<TermsResponse> getTerms(
            @Query("lang") String lang
    );
    @GET(Urls.About)
    Call<TermsResponse> getAbout(
            @Query("lang") String lang
    );
    @GET(Urls.AppInfo)
    Call<ContactUsResponse> getAppInfo(
            @Query("lang") String lang
    );
    @POST(Urls.Complain)
    Call<BaseResponse> sendComplain(
            @Query("user_id") String user_id,
            @Query("lang") String lang,
            @Query("title") String title,
            @Query("body") String body
    );
    @POST(Urls.Login)
    Call<LoginResponse> Login(
            @Query("lang") String lang,
            @Query("phone") String phone,
            @Query("password") String password,
            @Query("device") String device,
            @Query("device_id") String device_id
    );
    @POST(Urls.Logout)
    Call<BaseResponse> logout(
            @Query("user_id") int user_id,
            @Query("device_id") String device_id
    );
    @Multipart
    @POST(Urls.Register)
    Call<RegisterResponse>delegateRegister(
            @Query("lang") String lang,
            @Query("type") String type,
            @Query("name") String name,
            @Query("password") String password,
            @Query("email") String email,
            @Query("phone") String phone,
            @Query("city_id") String city_id,
            @Query("lat") String lat,
            @Query("lng") String lng,
            @Query("nationality_id") int nationality_id,
            @Query("civil_number") String civil_number,
            @Part MultipartBody.Part avatar
    );
    @POST(Urls.ConfirmCode)
    Call<ConfirmCodeModel> confirm(
            @Query("user_id") String user_id,
            @Query("code") String code
    );
//    @GET(Urls.Offers)
//    Call<SearchResponse> getOffers(
//            @Query("lang") String lang,
//            @Query("user_id") int user_id
//    );
//    @POST(Urls.Shops)
//    Call<ShopsResponse> getShops(
//            @Query("lang") String lang,
//            @Query("user_id") int user_id,
//            @Query("category_id") int category_id
//    );
//    @POST(Urls.ShowShop)
//    Call<ShopDetailsResponse> showShop(
//            @Query("lang") String lang,
//            @Query("user_id") int user_id,
//            @Query("shop_id") int shop_id
//    );
//    @POST(Urls.ProductsByCategory)
//    Call<ShopProductResponse> getProducts(
//            @Query("user_id") int user_id,
//            @Query("shop_id") int shop_id,
//            @Query("category_id") int category_id,
//            @Query("lang") String lang
//    );
    @POST(Urls.RateShop)
    Call<BaseResponse> DoRate(
            @Query("user_id") int user_id,
            @Query("shop_id") int shop_id,
            @Query("stars") int stars,
            @Query("comment") String comment
    );
//    @POST(Urls.GetComments)
//    Call<RatesResponse> getComments(
//            @Query("lang") String lang,
//            @Query("user_id") int user_id,
//            @Query("shop_id") int shop_id
//    );
//    @POST(Urls.ShowProduct)
//    Call<ProductDetailsResponse> showproduct(
//            @Query("user_id") int user_id,
//            @Query("product_id") int product_id
//    );
    @POST(Urls.AddToCart)
    Call<BaseResponse> addToCart(
            @Query("lang") String lang,
            @Query("user_id") int user_id,
            @Query("product_id") int product_id,
            @Query("product_count") int product_count,
            @Query("additions") String additions
    );
//    @POST(Urls.ShowCart)
//    Call<BasketResponse> myCart(
//            @Query("lang") String lang,
//            @Query("user_id") int user_id
//    );
    @POST(Urls.DeleteCartProduct)
    Call<BaseResponse> deleteCart(
            @Query("user_id") int user_id,
            @Query("cart_product_id") int cart_product_id
    );
    @POST(Urls.AddOrder)
    Call<BaseResponse> doOrder(
            @Query("lang") String lang,
            @Query("user_id") int user_id,
            @Query("way_type") int way_type
    );
//    @POST(Urls.ClientOrders)
//    Call<OrderResponse> getClientOrders(
//            @Query("lang") String lang,
//            @Query("user_id") int user_id
//    );
    @POST(Urls.ChangeOrderStatus)
    Call<BaseResponse> changeStatus(
            @Query("lang") String lang,
            @Query("user_id") int user_id,
            @Query("order_id") int order_id,
            @Query("status") int status
    );
    @POST(Urls.ShowNotification)
    Call<NotificationResponse> getNotifications(
            @Query("lang") String lang,
            @Query("user_id") int user_id
    );
    @POST(Urls.FinshedOrders)
    Call<NewOrderResponse> getFinishedOrders(
            @Query("lang") String lang,
            @Query("user_id") int user_id
    );
    @Multipart
    @POST(Urls.Update)
    Call<LoginResponse> clientUpdate(
            @Query("lang") String lang,
            @Query("user_id") String user_id,
            @Query("name") String name,
            @Query("email") String email,
            @Query("phone") String phone,
            @Query("city_id") String city_id,
            @Query("lat") String lat,
            @Query("lng") String lng,
            @Query("nationality_id") int nationality_id,
            @Query("civil_number") String civil_number,
            @Part MultipartBody.Part avatar
    );
    @POST(Urls.Update)
    Call<LoginResponse> clientUpdatewithoutImage(
            @Query("lang") String lang,
            @Query("user_id") String user_id,
            @Query("name") String name,
            @Query("email") String email,
            @Query("phone") String phone,
            @Query("city_id") String city_id,
            @Query("lat") String lat,
            @Query("lng") String lng,
            @Query("nationality_id") int nationality_id,
            @Query("civil_number") String civil_number

    );
    @POST(Urls.UpdatePassword)
    Call<BaseResponse> updatePass(
            @Query("user_id") String user_id,
            @Query("password") String password
    );
    @POST(Urls.ForgetPass)
    Call<ForgotPasswordResponse> forgetPass(
            @Query("phone") String phone
    );
    @POST(Urls.ForgetPassword)
    Call<NewPasswordResponse> forgetPassword(
            @Query("user_id") String user_id,
            @Query("code") String code
    );
    @GET(Urls.Profile)
    Call<LoginResponse> getProfile(
            @Query("lang") String lang,
            @Query("user_id") int user_id
    );
//    @POST(Urls.SearchProduct)
//    Call<SearchResponse> search(
//            @Query("lang") String lang,
//            @Query("user_id") int user_id,
//            @Query("category_id") int category_id,
//            @Query("city_id") int city_id,
//            @Query("price_list_id") int price_list_id,
//            @Query("lat") String lat,
//            @Query("lng") String lng
//    );
//    @GET(Urls.PriceList)
//    Call<PriceResponse> getPriceList(
//            @Query("lang") String lang
//    );
//    @POST(Urls.MyTracks)
//    Call<TrackResponse> getMyTracks(@Query("lang") String lang,
//                                    @Query("user_id") int user_id);
//    @GET(Urls.Tracks)
//    Call<TracksResponse> getTracks(@Query("lang") String lang);
//    @POST(Urls.BookTrack)
//    Call<BaseResponse> bookTrack(
//            @Query("lang") String lang,
//            @Query("user_id") int user_id,
//            @Query("book_day") String book_day,
//            @Query("book_time") String book_time,
//            @Query("lat") String lat,
//            @Query("lng") String lng,
//            @Query("address") String address,
//            @Query("name") String name,
//            @Query("phone") String phone,
//            @Query("category_id") String category_id,
//            @Query("persons") String persons,
//            @Query("shop_id") String shop_id,
//            @Query("type") String type
//    );
    @POST(Urls.MyDebt)
    Call<MyDebtRespose> getDebt(@Query("user_id") int user_id);
    @Multipart
    @POST(Urls.Transfer)
    Call<BaseResponse> transfer(@Query("lang") String lang,
                                @Query("user_id") int user_id,
                                @Query("type") int type,
                                @Query("bank_name") String bank_name,
                                @Query("acc_name") String acc_name,
                                @Query("bank_number") String bank_number,
                                @Query("iban") String iban,
                                @Query("money") String money,
                                @Part MultipartBody.Part image);
    @POST(Urls.NewOrders)
    Call<NewOrderResponse> getNewOrders(@Query("user_id") int id,
                                        @Query("lang") String lang);
    @POST(Urls.LoadingOrders)
    Call<NewOrderResponse> getLoading(@Query("user_id") int id,
                                        @Query("lang") String lang);
    @POST(Urls.OnWayOrders)
    Call<NewOrderResponse> getOnWay(@Query("user_id") int id,
                                      @Query("lang") String lang);
    @POST(Urls.CurrentOrders)
    Call<NewOrderResponse> getCurrent(@Query("user_id") int id,
                                    @Query("lang") String lang);

}

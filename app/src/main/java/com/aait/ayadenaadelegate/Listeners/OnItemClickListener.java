package com.aait.ayadenaadelegate.Listeners;

import android.view.View;

public interface OnItemClickListener {
    public void onItemClick(View view, int position);
}
